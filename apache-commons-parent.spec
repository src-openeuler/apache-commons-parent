Name:           apache-commons-parent
Version:        43
Release:        4
Summary:        Apache Commons Parent Pom
License:        ASL 2.0
URL:            https://commons.apache.org/commons-parent-pom.html
BuildArch:      noarch

Source0:        https://archive.apache.org/dist/commons/commons-parent/commons-parent-43-src.zip

BuildRequires:  maven-local mvn(org.apache:apache:pom:) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:  mvn(org.codehaus.mojo:build-helper-maven-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-assembly-plugin)
BuildRequires:  mvn(org.codehaus.mojo:build-helper-maven-plugin)
Requires:       mvn(org.codehaus.mojo:build-helper-maven-plugin)

%description
Project object model file for the apache-commons package.
commons-parent is the parent pom for the components' Maven build.

%prep
%autosetup  -n commons-parent-%{version}-src -p1

%pom_remove_plugin org.apache.commons:commons-build-plugin
%pom_remove_plugin org.apache.maven.plugins:maven-scm-publish-plugin
for file in apache-rat-plugin buildnumber-maven-plugin maven-enforcer-plugin maven-site-plugin; do
        %pom_remove_plugin :$file
done

for profile in animal-sniffer japicmp jacoco cobertura clirr; do
    %pom_xpath_remove "pom:profile[pom:id='$profile']"
done

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc RELEASE-NOTES.txt
%license LICENSE.txt NOTICE.txt

%changelog
* Fri Feb 21 2020 yangjian<yangjian79@huawei.com> - 43-4
- Package init
